import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import About from '@/components/About'
import Search from '@/components/Search'
import WeatherDetial from '@/components/WeatherDetial'

Vue.use(Router)
export default new Router({

  routes: [
    {
      path: '/',
      name: Home,
      component: Home
    },
    {
      path: '/about',
      name: About,
      component: About
    },
    {
      path: '/search/:keyword',
      name: Search,
      component: Search
    },
    {
      path: '/weather/:woied',
      name: WeatherDetial,
      component: WeatherDetial
    }
  ]
})