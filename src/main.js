import Vue from 'vue'
import App from './App.vue'
import router from './routes'
// Import Installed Libraries

// Bootstrap-vue
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(BootstrapVue)

// Axios
import Axios from 'axios'
// Importing Axios globally
Vue.prototype.$http = Axios

// Moment.JS
Vue.use(require('vue-moment'))
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
